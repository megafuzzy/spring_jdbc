package com.buutcamp.main;

import com.buutcamp.databaselogic.ClientInfoDao;
import com.buutcamp.objects.ClientInfo;
import com.mysql.cj.xdevapi.Client;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class RunApp {

    public RunApp() {

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        ClientInfoDao clientInfoDao = (ClientInfoDao)ctx.getBean("clientInfoDao");

//        System.out.println(list.get(0).getFirstName()+"\n");
//        System.out.println(list.get(0).getLastName()+"\n");
//        System.out.println(list.get(0).getAge()+"\n");

        ClientInfo clientInfo = new ClientInfo("Firstname", "Lastname", 100);
        boolean successful = clientInfoDao.createRow(clientInfo);

        List<ClientInfo> list = clientInfoDao.getAllData();

        for(int i = 0; i < list.size(); i++) {

            System.out.println(list.get(i).getId());
            System.out.println(list.get(i).getFirstName());
            System.out.println(list.get(i).getLastName());
            System.out.println(list.get(i).getAge());
            System.out.println("\n");
        }

        ctx.close();
        //if (successful) {


        //}
    }
}
